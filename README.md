# Single cell RNAseq workshop

Originally for IGMM scRNAseq day. Uses 10X PBMC data.


Based on [osca](https://osca.bioconductor.org).

Extra resources:
  - [scran](https://bioconductor.org/packages/release/bioc/html/scran.html)
  - [scater](https://bioconductor.org/packages/release/bioc/html/scater.html)
  - [JM gitlab](https://jmlab-gitlab.cruk.cam.ac.uk/teaching/scTeachingMaterials)

Covers:
- Pre-processing
  - Publicly-available data
  - Loading data
  - Empty drops
  - Cell QC
  - Gene QC
- Normalisation
- Feature selection
  - Variance of log-counts
  - Coefficient of variation of normalised counts
  - Variance decomposition using spike-in genes
  - Selecting HVGs based on fitted trends
  - *A priori* genes of interest
- Doublet barcodes
- Dimensionality reduction
  - Principal Component Analysis (PCA)
    - Selecting PCs
    - Elbow point
    - Jackstraw
    - Technical noise
    - Population structure
    - Interpreting principal components
  - Non-linear dimensionality reduction
    - t-SNE
    - UMAP
- Clustering
    - Edge weighting
    - Clustering algorithms
    - Other clustering methods
      - K-means
      - Hierarchical clustering
  - Differential expression
    - Visualising marker genes
      - One at a time
      - Many at once
    - Validity of p-values
    - Dealing with multiple batches or samples

