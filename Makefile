scWorkshop.html: scWorkshop.Rmd raw_feature_bc_matrix
	Rscript -e 'rmarkdown::render("$<");'

raw_feature_bc_matrix:
	wget http://cf.10xgenomics.com/samples/cell-exp/3.0.2/5k_pbmc_v3/5k_pbmc_v3_raw_feature_bc_matrix.tar.gz && \
		tar -xzf 5k_pbmc_v3_raw_feature_bc_matrix.tar.gz
